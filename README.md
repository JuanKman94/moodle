# Quique's Moodle

Use `docker-compose` to run the project:

```bash
$ sudo docker-compose up -d moodle
$ xdg-open http://localhost
```

## Resources

* [Auto deployment using git](https://github.com/olipo186/Git-Auto-Deploy)

Example configuration for this project

```bash
$ cat /etc/git-auto-deploy.conf.json
...
  // Deploy commands that should be executed for all projects
  "global_deploy": [
    "echo Deploy started!",
    "/usr/local/bin/moodle-ci"
  ],

  // Project configs
  "repositories": [
    {
      "url": "https://gitlab.com/JuanKman94/moodle.git",
      "branch": "master",
      "remote": "origin",
      "path": "/srv/moodle-site",
      "deploy": "echo 'Deploying moodle'"
    }
...
$ cat /usr/local/bin/moodle-ci
#!/bin/sh

echo "$0: setting up production variables"

MOODLE_DIR="${MOODLE_DIR:-/srv/moodle-site}"
PROD_HOSTNAME="178.128.9.236"

sed -i "s|http://localhost|http://${PROD_HOSTNAME}|" "${MOODLE_DIR}/config.php"
sed -i "s|\($CFG->dbhost\s*\)= 'db'|\1 = 'localhost'|" "${MOODLE_DIR}/config.php"
sed -i "s|'redis'|'localhost'|" "${MOODLE_DIR}/config.php"

echo "$0: Done!"
```
