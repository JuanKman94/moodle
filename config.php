<?php  // Moodle configuration file

unset($CFG);
global $CFG;
$CFG = new stdClass();

//$CFG->dbtype    = 'mysqli';
$CFG->dbtype    = 'mariadb';
$CFG->dblibrary = 'native';
$CFG->dbhost    = 'db';
$CFG->dbname    = 'moodle';
$CFG->dbuser    = 'moodle';
$CFG->dbpass    = 'hey,itsasecret';
$CFG->prefix    = '';
$CFG->dboptions = array (
    'dbpersist' => 0,
    'dbport' => '',
    'dbsocket' => '',
    'dbcollation' => 'utf8mb4_unicode_ci',
);

$CFG->wwwroot   = 'http://localhost';
$CFG->dataroot  = '/srv/moodle-data';
$CFG->admin     = 'admin';

$CFG->directorypermissions = 0777;

// if the handler is not created from the admin portal
// then this causes the application to crash
//$CFG->session_handler_class = '\core\session\redis';
//$CFG->session_redis_host = 'redis';
//$CFG->session_redis_port = 6379;
//$CFG->session_redis_database = 0;
//$CFG->session_redis_prefix = 'myschool_us_';
//$CFG->session_redis_acquire_lock_timeout = 120;
//$CFG->session_redis_lock_expire = 7200;

require_once(__DIR__ . '/lib/setup.php');
